# SurplusMed

To deploy on your own server. You must have the following requirements:

- Laravel
- Composer
- php 7.3 
- mySQL

After that, you must simply open your terminal and run the follwoing command in 
the project directory:

`php artisan serve`