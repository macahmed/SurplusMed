<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class EditUserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        $user->fill($request->input());
        $user->save();

        return redirect('/');
    }
}
