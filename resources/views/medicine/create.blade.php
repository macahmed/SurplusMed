@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create Medicine') }}</div>

                <div class="card-body">
                    <form method="POST" action="/medicine">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" name="name" required autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Brand Name') }}</label>

                            <div class="col-md-6">
                                <input id="brand_name" type="text" class="form-control @error('brand_name') is-invalid @enderror" value="{{ old('brand_name') }}" name="brand_name" required autofocus>

                                @error('brand_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="pack_size" class="col-md-4 col-form-label text-md-right">{{ __('Pack Size') }}</label>

                            <div class="col-md-6">
                                <input id="pack_size" type="text" class="form-control @error('pack_size') is-invalid @enderror"  value="{{ old('pack_size') }}" name="pack_size" required autofocus>

                                @error('pack_size')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="batch_number" class="col-md-4 col-form-label text-md-right">{{ __('Batch Number') }}</label>

                            <div class="col-md-6">
                                <input id="batch_number" type="text" class="form-control @error('batch_number') is-invalid @enderror" value="{{ old('batch_number') }}" name="batch_number" required autofocus>

                                @error('batch_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('Quantity') }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control @error('quantity') is-invalid @enderror" value="{{ old('quantity') }}" name="quantity" required autofocus>

                                @error('quantity')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price_per_pack" class="col-md-4 col-form-label text-md-right">{{ __('Price Per Pack') }}</label>

                            <div class="col-md-6">
                                <input id="price_per_pack" type="text" class="form-control @error('price_per_pack') is-invalid @enderror" value="{{ old('price_per_pack') }}" name="price_per_pack" required autofocus>

                                @error('price_per_pack')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="expiry_date" class="col-md-4 col-form-label text-md-right">{{ __('Expiry Date') }}</label>

                            <div class="col-md-6">
                                <input id="expiry_date" type="date" class="form-control @error('expiry_date') is-invalid @enderror" value="{{ old('expiry_date') }}" name="expiry_date" required autofocus>

                                @error('expiry_date')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="manufacturer" class="col-md-4 col-form-label text-md-right">{{ __('Manufacturer') }}</label>

                            <div class="col-md-6">
                                <input id="manufacturer" type="text" class="form-control @error('manufacturer') is-invalid @enderror" value="{{ old('manufacturer') }}" name="manufacturer" required autofocus>

                                @error('manufacturer')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="method_of_delivery" class="col-md-4 col-form-label text-md-right">{{ __('Method of Delivery') }}</label>

                            <div class="col-md-6">
                                <input id="method_of_delivery" type="text" class="form-control @error('method_of_delivery') is-invalid @enderror" value="{{ old('method_of_delivery') }}" name="method_of_delivery" required autofocus>

                                @error('method_of_delivery')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary float-right">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection
