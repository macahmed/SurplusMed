@extends('layouts.app')

@section('content')
    <div class="container list">
        <table class="table table-hover table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Brand Name</th>
                    <th scope="col">Pack Size</th>
                    <th scope="col">Batch Number</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price Per Pack</th>
                    <th scope="col">Expiry Date</th>
                    <th scope="col">Manufacturer</th>
                    <th scope="col">Delivery Method</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($medicines as $medicine)
                    <tr>
                        <td scope="row">{{$medicine->name}}</td>
                        <td scope="row">{{$medicine->brand_name}}</td>
                        <td scope="row">{{$medicine->pack_size}}</td>
                        <td scope="row">{{$medicine->batch_number}}</td>
                        <td scope="row">{{$medicine->quantity}}</td>
                        <td scope="row">{{$medicine->price_per_pack}}</td>
                        <td scope="row">{{$medicine->expiry_date}}</td>
                        <td scope="row">{{$medicine->manufacturer}}</td>
                        <td scope="row">{{$medicine->method_of_delivery}}</td>
                    </tr>
                    @empty
                        <h1>No Medicines Listed!</h1>
                    @endforelse
                </tbody>
        </table>
    </div>
@endsection