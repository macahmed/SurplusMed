@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('edit.user') }}">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <br>

                        <div class="form-group row">
                            <label for="gphc_number" class="col-md-4 col-form-label text-md-right">{{ __('GHPC Number') }}</label>

                            <div class="col-md-6">
                                <input id="gphc_number" type="text" class="form-control @error('gphc_number') is-invalid @enderror" name="gphc_number" value="{{ Auth::user()->gphc_number }}" required autocomplete="gphc_number">

                                @error('gphc_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hospital_name" class="col-md-4 col-form-label text-md-right">{{ __('Hospital Name') }}</label>

                            <div class="col-md-6">
                                <input id="hospital_name" type="text" class="form-control @error('hospital_name') is-invalid @enderror" name="hospital_name" value="{{ Auth::user()->hospital_name }}" required autocomplete="hospital_name">

                                @error('hospital_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="wholesaler_dealer_number" class="col-md-4 col-form-label text-md-right">{{ __('Wholesaler Dealer Number') }}</label>

                            <div class="col-md-6">
                                <input id="wholesaler_dealer_number" type="text" class="form-control @error('wholesaler_dealer_number') is-invalid @enderror" name="wholesaler_dealer_number" value="{{ Auth::user()->wholesaler_dealer_number }}" required autocomplete="wholesaler_dealer_number">

                                @error('wholesaler_dealer_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hospital_delivery_address" class="col-md-4 col-form-label text-md-right">{{ __('Hospital Delivery Address') }}</label>

                            <div class="col-md-6">
                                <input id="hospital_delivery_address" type="text" class="form-control @error('hospital_delivery_address') is-invalid @enderror" value="{{ Auth::user()->hospital_delivery_address }}" name="hospital_delivery_address"  required autocomplete="hospital_delivery_address">

                                @error('hospital_delivery_address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hospital_billing_address" class="col-md-4 col-form-label text-md-right">{{ __('Hospital Billing Address') }}</label>

                            <div class="col-md-6">
                                <input id="hospital_billing_address" type="text" class="form-control @error('hospital_billing_address') is-invalid @enderror" value="{{ Auth::user()->hospital_billing_address }}" name="hospital_billing_address" required autocomplete="hospital_billing_address">

                                @error('hospital_billing_address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile_no" class="col-md-4 col-form-label text-md-right">{{ __('Mobile No') }}</label>

                            <div class="col-md-6">
                                <input id="mobile_no" type="text" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no" value="{{ Auth::user()->mobile_no }}" required autocomplete="mobile_no">

                                @error('mobile_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary float-right">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection
